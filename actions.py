import util

def action_charge(pirate, game, **kwargs):
    '''
    Parameters
    ----------
    pirate : PirateDecorator
        mostly our pirate
    game : Game
        game
    charge_targets : list<Pirate>
        List of mostly enemy pirates we want to charge
    charge_threshold : int
        radius
    '''
    if not pirate.has_push():
        return
    targets = kwargs['charge_targets']
    charge_threshold = kwargs['charge_threshold']
    # Sort targets by their distance to the pirate
    closest_targets = util.closest_locobj(targets, pirate, max_distance=charge_threshold)
    # If the closest one is within the charge distance, sail towards him
    if len(closest_targets) > 0:
        pirate.sail(closest_targets[0])

def action_push(pirate, game, **kwargs):
    ''' If possible, push the first pushable 
    pirate in push_targets
    Parameters
    ----------
    pirate : PirateDecorator
        mostly our pirate
    game : Game
        game
    push_targets : list<Pirate>
        List of mostly enemy pirates we want to push
    max_push : int
        what is the maximum amount of times this 
        thing will be pushed this turn. (default 1)
    '''
    if not pirate.has_push():
        return
    push_targets = kwargs['push_targets']
    for target in push_targets:
        if pirate.can_push(target, kwargs['max_push'] if 'max_push' in kwargs.keys() else 1):
            pirate.push(target, kwargs['push_destination'], kwargs['max_push'] if 'max_push' in kwargs.keys() else 1)
            break

def action_move(pirate, game, **kwargs):
    ''' Uses pirate.sail
    Parameters
    ----------
    pirate : PirateDecorator
        mostly our pirate
    game : Game
        game
    destination : Location
        location to sail to
    '''
    destination = kwargs['destination']
    pirate.sail(destination)