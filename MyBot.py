import tactics
import states
import util
import pirates
import traceback
import turns 
import globalvars

class PirateDecorator(object):
    def __init__(self, pirate):
        ''' Initialize a pirate decorator
        Parameters
        ----------
        self : PirateDecorator
            self
        pirate : Pirate
            pirate
        '''
        self.pirate = pirate
        self.free = True
        self.actions = []

    def set_actions(self, actions):
        self.actions = actions

    def run_actions(self, game, **kwargs):
        ''' Run tactics in order until pirate is performing one
        '''
        for action in self.actions:
            if not self.is_free():
                break
            action(self, game, **kwargs)
    
    def has_capsule(self):
        return self.pirate.has_capsule()

    def has_push(self):
        return self.pirate.push_reload_turns == 0

    def can_push(self, pushable_mapobj, max_push = 1):
        ''' Execute a can_push command on a pirate.
        Parameters
        ----------
        self : PirateDecorator
            self
        pushable_mapobj : SpacePbject
            the thing to push
        max_push : int
            what is the maximum amount of times this 
            thing will be pushed this turn. (default 1)
        Returns
        -------
        False if can_push returned false or the thing was already 
        pushed to many times this turn, True otherwise.
        '''
        tm = globalvars.turn_manager
        return self.pirate.can_push(pushable_mapobj) and tm.was_mapobj_pushed(pushable_mapobj, max_push)
    
    def sail(self, destination):
        self.pirate.sail(destination)
        self.set_busy()

    def push(self, pushable_mapobj, destination, max_push = 1):
        ''' Execute a push command on a pirate at the end of the turn.
        Parameters
        ----------
        self : PirateDecorator
            self
        pushable_mapobj : SpacePbject
            the thing to push
        destination : Location
            the location to push the thing towards
        max_push : int
            what is the maximum amount of times this 
            thing will be pushed this turn. (default 1)
        '''
        tm = globalvars.turn_manager
        if self.can_push(pushable_mapobj, max_push):
            self.pirate.push(pushable_mapobj, destination)
            self.set_busy()
            tm.set_push(pushable_mapobj)

    def distance(self, another):
        return self.pirate.distance(another)

    def in_range(self, other, range_):
        return self.pirate.in_range(other, range_)

    def is_free(self):
        return self.free

    def get_location(self):
        return self.pirate.location

    def set_busy(self):
        self.free = False

    def get_id(self):
        return self.pirate.id

# Cluster class
#   pirates- list of pirates that are forming the cluster
#   tactics- list of functions with the following signature:
#       `def tactic_tacticname(pirates, game, **kwargs)`
class Cluster(list):
    def __init__(self, entities):
        self.entities = map(PirateDecorator, entities)
        
    def __getitem__(self, item):
        '''Access cluster like an array
        '''
        return self.entities[item]

    def __repr__(self):
        return ', '.join(["Pirate #"+str(pd.pirate.id) for pd in self.entities])

    def has_capsule(self):
        for entity in self.entities:
            if entity.has_capsule():
                return True
        return False

    def add_entity(self, entity):
        self.entities.append(PirateDecorator(entity))

    def set_common_actions(self, actions):
        ''' Set the same actions for all the entities in the cluster
        '''
        for entity in self.entities:
            entity.set_actions(actions)        

    def run_actions(self, game, **kwargs): 
        ''' Run actions of every entity in the cluster, 
        passing the cluster, game and additional arguments
        '''
        for entity in self.entities:
            entity.run_actions(game, **kwargs) 

    def entities_count(self):
        return len(self.entities)

    def get_center(self):
        from util import center_of_mass
        return center_of_mass(map(lambda pd: pd.pirate, self.entities))

    def get_free_entities(self):
        return filter(lambda entity: entity.is_free(), self.entities)

    def is_gathered(self):
        is_gathered = True
        for entity in self.entities:
            if entity.get_location().distance(self.get_center()) > 0:
                is_gathered = False
        return is_gathered

    def free_pirates_iter(self):
        '''
        Returns
        -------
        Returns an iterator for the free pirates in the cluster
        '''
        for entity in self.entities:
            if entity.is_free():
                yield entity

def attack(game, cluster):
    tactics.tactic_repel_asteroid(cluster, game)
    tactics.tactic_avoid_asteroid(cluster, game)
    tactics.tactic_gather(cluster, game, cluster.get_center())
    if len(game.get_my_capsules()) > 0:
        tactics.tactic_take_capsule(cluster, game, util.choose_capsule(game, cluster))
    # tactics.tactic_bridge(cluster, game)
    tactics.tactic_score(cluster, game, util.get_closest_mothership(game, cluster.get_center()))

def defend(game, cluster, mothership):
    tactics.tactic_repel_asteroid(cluster, game)
    tactics.tactic_avoid_asteroid(cluster, game)
    tactics.tactic_destroy_wormhole(cluster, game, mothership)
    tactics.tactic_defend_location(cluster, game, mothership.location)

def do_turn(game):
    try:
        globalvars.initvars(game)

        pirates = game.get_my_living_pirates()
        clusters = util.into_clusters(pirates, round(len(pirates)/2), minsize=2)

        mothership_to_defend = None
        average_distance = 10000
        for enemy_mothership in game.get_enemy_motherships():
            enemies = util.get_threating_enemies(game, enemy_mothership)
            enemies_count = len(enemies)
            if enemies_count > 0:
                total_distance = reduce(lambda x, y: x+y[1], enemies, 0)
                distance = total_distance/enemies_count
                if distance < average_distance:
                    average_distance = distance
                    mothership_to_defend = enemy_mothership
        
        defenders = []
        for cluster in clusters:
            if cluster.has_capsule():
                attack(game, cluster)
            else:
                defenders.append(cluster)

        idle = defenders
        if mothership_to_defend:
            print "Defending mothership {0}".format(mothership_to_defend)
            close_clusters = sorted(defenders, key=lambda cluster: cluster.get_center().distance(mothership_to_defend))
            closest_cluster = close_clusters[0]
            defend(game, closest_cluster, mothership_to_defend)
            idle = close_clusters[1:]

        for cluster in idle:
            attack(game, cluster)
    except:
        print traceback.format_exc()
