from math import sqrt
import pirates 
import util

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __repr__(self):
        return "Point(%s, %s)"%(self.x, self.y)


# Vector class
class Vector:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __repr__(self):
        return "Vector(%s,%s)"%(self.x, self.y)

    @classmethod
    def from_point(cls, point):
        return Vector(point.x, point.y)

    def unit(self):
        magnitude = self.length()
        if magnitude == 0:
            return None
        return Vector(self.x/magnitude, self.y/magnitude)

    def add(self, vector):
        if isinstance(vector, Vector):
            return Vector(self.x+vector.x, self.y+vector.y)

    def negative(self):
        return Vector(-self.x, -self.y)

    def subtract(self, vector):
        if isinstance(vector, Vector):
            return self.add(vector.negative())

    def scalar(self, vector):
        if isinstance(vector, Vector):
            return self.x*vector.x + self.y*vector.y

    def multiply(self, scalar):
        return Vector.from_point(Point(self.x*scalar, self.y*scalar))

    def length(self):
        return sqrt(self.x*self.x + self.y*self.y)


class Line:
    def __init__(self, point, vector):
        self.point = point
        self.vector = vector

    def __repr__(self):
        return "%s + t%s"%(self.point, str(self.vector)[6:])

    def location_on_line(self, location, distance):
        return len(self.locations_near_line([location], distance)) > 0

    def get_in_t(self, t):
        from pirates import Location
        x = self.point.x + self.vector.x*t
        y = self.point.y + self.vector.y*t
        return Location(int(y), int(x))

    def locations_near_line(self, locations, distance):
        nearby_locations = []
        for location in locations:
            p = Point(location.location.col, location.location.row)
            d = self.get_normal_from_point(p)
            if d.length() < distance:
                p_on_line = d.add(Vector.from_point(p))
                # Check if the T is positive, if it is, the entity is moving towards the location
                t_x = 1
                t_y = 1
                # Beware of division by zero
                if self.vector.x != 0:
                    t_x = (p_on_line.x - self.point.x)/self.vector.x
                if self.vector.y != 0:
                    t_y = (p_on_line.y - self.point.y)/self.vector.y
                if t_x < 0 or t_y < 0:
                    continue
                nearby_locations.append(location)
        return nearby_locations

    def get_normal_from_point(self, p):
        a_minus_p = Vector.from_point(self.point).subtract(Vector.from_point(p))
        factor = a_minus_p.scalar(self.vector)
        d = a_minus_p.subtract(self.vector.multiply(factor))
        return d

    def get_normal_from_object(self, mapobj):
        return self.get_normal_from_point(Point(mapobj.location.col, mapobj.location.row))


class State:
    MOVES_COUNT = 2
    
    def __init__(self):
        self.moves = []
    
    def update_move(self, move):
        # Keep the queue size at 2 or 3 items
        # First item is the latest move
        self.moves.insert(0, move)
        if len(self.moves) > State.MOVES_COUNT:
            del self.moves[-1]
    
    def get_direction(self):
        if len(self.moves) >= 2:
            point2 = Point(self.moves[0].col, self.moves[0].row)
            point1 = Point(self.moves[1].col, self.moves[1].row)
            # Create vector out of the previous 3 moves and return a vector
            direction_vector = Vector.from_point(point2).subtract(Vector.from_point(point1))
            unit_vector = direction_vector.unit()
            if unit_vector:
                l = Line(point1, unit_vector)
                return l

class StateManager:
    def __init__(self):
        self.my_states = {}
        self.enemy_states = {}
        self.astroids_states = {}

    def populate_game_state(self, game):
        self.populate_states(game.get_all_my_pirates(), self.my_states)
        self.populate_states(game.get_all_enemy_pirates(), self.enemy_states)
        self.populate_states(game.get_all_asteroids(), self.astroids_states)
    
    def entity_pushed(self, entity_id):
        pass

    def populate_states(self, entities, states_dict):
        # Loop over enemy_living_pirates
        # Add State if doesnt exists, else update_move with the enemy's current location
        for entity in entities:
            if entity.is_alive():
                if entity.id not in states_dict.keys():
                    states_dict[entity.id] = State()
                states_dict[entity.id].update_move(entity.location)
            elif entity.id in states_dict.keys():
                del states_dict[entity.id] 

    def get_enemies_heading_to(self, game, location):
        threating_enemies_ids = []
        for (enemy_id, enemy_state) in self.enemy_states.items():
            direction = enemy_state.get_direction()
            if direction:
                # If the destination is on route of the enemy, add it to the list
                if direction.location_on_line(location, 1000):
                    threating_enemies_ids.append(enemy_id)
        threating_enemies = util.get_entities_with_ids(game.get_enemy_living_pirates(), threating_enemies_ids)
        return threating_enemies

    def populate_states(self, entities, states_dict):
        # Loop over enemy_living_pirates
        # Add State if doesnt exists, else update_move with the enemy's current location
        for entity in entities:
            if entity.is_alive():
                if entity.id not in states_dict.keys():
                    states_dict[entity.id] = State()
                states_dict[entity.id].update_move(entity.location)
            elif entity.id in states_dict.keys():
                del states_dict[entity.id] 

    def get_state_of_asteroid(self, asteroid_id):
        if asteroid_id in self.astroids_states.keys():
            return self.astroids_states[asteroid_id]
        return None

    def get_asteroids_colliding_with(self, game, entity):
        colliding_asteroids_ids = []
        for (asteroid_id, state) in self.astroids_states.items():
            # If the function returns entity, the astroid is on a route to collide with it 
            route = state.get_direction()
            if route:
                if route.location_on_line(entity, game.asteroid_size+100):
                    colliding_asteroids_ids.append(asteroid_id)
        colliding_asteroids = util.get_entities_with_ids(game.get_living_asteroids(), colliding_asteroids_ids)
        return colliding_asteroids

    def get_asteroid_route(self, asteroid_id):
        return self.astroids_states[asteroid_id].get_direction()
    
    def get_cluster_average_route(self, cluster):
        direction_vector = Vector(0, 0)
        entities = cluster.entities
        for entity in entities:
            direction = self.my_states[entity.pirate.id].get_direction()
            if direction:
                direction_vector = direction_vector.add(direction.vector)
        direction_vector.x /= len(entities)
        direction_vector.y /= len(entities)
        center_location = cluster.get_center()
        return Line(Point(center_location.col, center_location.row), direction_vector)
