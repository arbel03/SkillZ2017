# from actions import *
# from util import *
import actions
import util
import globalvars

def tactic_take_capsule(cluster, game, capsule):
    ''' Send the cluster to the capsule
    '''
    # If we have a capsule, continue
    if cluster.has_capsule() or not capsule:
        return
    cluster.set_common_actions([actions.action_move])
    # TODO: Change it to the closest capsule to the specific cluster
    cluster.run_actions(game, destination=capsule)

def tactic_gather(cluster, game, location):
    enemy_close = False
    for pirate in game.get_enemy_living_pirates():
        if pirate.distance(location) < 1000:
            enemy_close = True
            break
    if cluster.is_gathered() or enemy_close or cluster.has_capsule():
        return
    cluster.set_common_actions([actions.action_move])
    cluster.run_actions(game, destination=location)

def tactic_repel_asteroid(cluster, game):
    # find general direction of the cluster and repel the astroid 
    # IN ANY DIRECTION EXCEPT THE REVERSE VECTOR TO THAT DIRECTION
    # elsewise the astroid will kill the cluser/pirates
    sm = globalvars.state_manager
    from states import Line
    cluster_direction = sm.get_cluster_average_route(cluster)
    cluster_negative_direction = Line(cluster_direction.point, cluster_direction.vector.negative())
    
    push_destinations = util.get_closest_capsules(game, cluster.get_center())
    push_destinations = sorted(push_destinations, key=lambda destination: destination.distance(cluster.get_center()))
    push_destinations.append(cluster_direction.get_in_t(1000))

    if len(push_destinations) == 0:
        return
    cluster.set_common_actions([actions.action_push])
    cluster.run_actions(game, push_destination=push_destinations[0], push_targets=game.get_living_asteroids(), max_push=1)


def tactic_avoid_asteroid(cluster, game):
    from pirates import Location
    sm = globalvars.state_manager
    for pirate in cluster.free_pirates_iter():
        colliding_asteroids = sm.get_asteroids_colliding_with(game, pirate.pirate)
        if len(colliding_asteroids) > 0:
            from states import Vector
            # Getting the closest asteriod to the cluster, this is the one we should avoid
            # Getting the closest asteroid
            colliding_asteroid = sorted(colliding_asteroids, key=lambda asteroid: asteroid.distance(pirate))
            route = sm.get_asteroid_route(colliding_asteroid[0].id)
            normal = route.get_normal_from_object(pirate.pirate).negative().multiply(10000)
            # Adding that vector to the current location of each pirate.
            end_point = normal.add(Vector(pirate.get_location().col, pirate.get_location().row))
            # Moving each pirate to that point.
            pirate.sail(Location(int(end_point.y), int(end_point.x)))

def tactic_defend_location(cluster, game, location):
    ''' Closest line of defense to the mothership
    Parameters
    ----------
    pirates : list
        list of pirates the action should be preformed on
    game : GameObject
        game object
    defend_location : Location
        where to defend
    range_from_location : int
        radius
    '''
    closest_capsule = util.get_closest_capsules(game, cluster.get_center())[0]
    
    cluster.set_common_actions([actions.action_push, actions.action_charge, actions.action_move])
    loc = location.towards(closest_capsule, 1000)  # TODO: Change hard coded value
    push_destination = None
    targets = None
    
    if closest_capsule.holder:
        targets = [closest_capsule.holder]
        push_destination = util.push_destination_to_edge(game, cluster.get_center(), has_capsule=True)
    else:
        targets = game.get_enemy_living_pirates()
        push_destination = util.push_destination_to_edge(game, cluster.get_center(), has_capsule=False)
    
    charge_threshold = ((game.cols+game.rows)/2)/4.23 # about 1500 if the size is 6350
    
    free_entities = cluster.get_free_entities()
    print free_entities
    # One third of the destroyers are rushing towards the enemy capsule
    rushers_amount = min(len(free_entities), game.num_pushes_for_capsule_loss)
    rushers = free_entities[:rushers_amount]
    print "Rushers: " + str(rushers)
    pushers = free_entities[rushers_amount:]
    print "Pushers: " + str(pushers)
    if rushers:
        # Win OneManArmy
        if len(game.get_all_my_pirates()) == 1:
            charge_threshold = 0
            loc = location.towards(closest_capsule, game.mothership_unload_range-100)
            targets = filter(None, [closest_capsule.holder])
        arguments_rushers = dict(charge_targets=targets, push_targets=targets, charge_threshold=charge_threshold, destination=loc, push_destination=push_destination, max_push=game.num_pushes_for_capsule_loss)
        for rusher in rushers:
            rusher.run_actions(game, **arguments_rushers)

    if pushers:
        push_destination = util.center_of_mass(map(lambda pd: pd.pirate, rushers)) if len(rushers) > 0 else push_destination
        charge_threshold = charge_threshold/2
        loc = loc.towards(location, game.push_range)
        arguments_pushers = dict(charge_targets=targets, push_targets=targets, charge_threshold=charge_threshold, destination=loc, push_destination=push_destination, max_push=game.num_pushes_for_capsule_loss)
        for pusher in pushers:
            pusher.run_actions(game, **arguments_pushers)

def tactic_bridge(cluster, game):
    return

def tactic_destroy_wormhole(cluster, game, mothership):
    # if a wormhole is close to our base, fuck it hard
    # TODO - make this smarter
    radius = game.wormhole_range + game.push_range + game.push_distance + mothership.unload_range + 100
    wormhole = util.closest_locobj(game.get_all_wormholes(), mothership.location, amount=1, max_distance=radius)
    if not wormhole:
        return
    else:
        wormhole = wormhole[0]
    print 'wormhole found', wormhole
    # make half of the cluster push the wormhole as far away as possible
    free_entities = cluster.get_free_entities()
    if free_entities and len(free_entities) >= 2:
        free_entities = free_entities[:int(len(free_entities)/2)]
        my_capsules = util.get_closest_my_capsules(game, wormhole.location)
        if my_capsules:
            push_destination = wormhole.location.towards(my_capsules[0], game.push_distance*len(free_entities))
        else:
            loc = util.get_farthest_wall(game, wormhole)
            push_destination = wormhole.location.towards(loc, game.push_distance*len(free_entities))
        # TODO - update center_of_mass function in util to get center_of_mass for PirateDecorator
        free_entities_center = None
        row = 0
        col = 0
        for entity in free_entities:
            row += entity.get_location().row
            col += entity.get_location().col
        free_entities_center = pirates.Location(row/len(free_entities), col/len(free_entities))
        arguments_pushers = dict(push_targets=[wormhole], push_destination=push_destination, max_push=1000, destination=wormhole.location.towards(free_entities_center, game.push_range))
        for p in free_entities:
            p.set_actions([actions.action_push, actions.action_move])
            p.run_actions(game, **arguments_pushers)

def tactic_score(cluster, game, mothership):
    # TODO: Assign every charging_pirate to every defending pirate using charge_targets and push_targets
    # Add a pirate that pushes the pirate with the capsule and scores a point
    # Add a delay action or a follow action - the capsule and booster pirate follow the charging_pirates by kwargs['distance']
    if not cluster.has_capsule():
        return
    
    charging_pirates = []
    for pd in cluster.free_pirates_iter():
        charging_pirates.append(pd)
    
    targets = util.closest_locobj(game.get_enemy_living_pirates(), mothership, max_distance=1500)
    for charging_pirate in charging_pirates:
        defender_to_attack = []
        if len(targets) != 0:
            target = targets.pop()
            targets.insert(0, target)
            defender_to_attack.append(target)
        charging_pirate.set_actions([actions.action_push, actions.action_charge, actions.action_move])
        push_destination = util.push_destination_to_edge(game, charging_pirate.get_location(), False)
        charging_pirate.run_actions(game, charge_targets=defender_to_attack, push_targets=defender_to_attack, charge_threshold=1000, destination=mothership, push_destination=push_destination)
    
