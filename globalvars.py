import states
import turns
def initvars(game):
    # Initializing our state_manager and turn_manager class, dont touch
    # =================================================================
    if 'state_manager' not in globals().keys():
        globals()['state_manager'] = states.StateManager()
    globals()['state_manager'].populate_game_state(game)
    globals()['turn_manager'] = turns.TurnManager(game)
    # =================================================================