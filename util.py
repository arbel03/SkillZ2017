import pirates
import globalvars

def into_clusters(pirates, maxsize, minsize=-1):
    from MyBot import Cluster
    ''' Make a cluster
    Parameters
    ----------
    pirates : List
        pirates
    '''
    clusters = []
    free_pirates = set(pirates)
    while len(free_pirates) > 0:
        pirate = free_pirates.pop()
        cluster = [pirate]
        while (len(cluster) < minsize or len(cluster) < maxsize) and len(free_pirates) > 0:
            center = center_of_mass(cluster)
            closest_pirate = closest_locobj(free_pirates, center, 1)[0]
            cluster.append(closest_pirate)
            free_pirates = free_pirates-set(cluster)
        clusters.append(Cluster(cluster))
    return clusters

def choose_capsule(game, cluster):
    """ Get an unoccupied capsule
    game : GameObject
        game object
    cluster : Cluster
        optimize capsule to cluster
    """
    capsules = closest_locobj(game.get_my_capsules(), cluster.get_center())
    for capsule in capsules:
        if not capsule.holder:
            return capsule.initial_location
    return capsules[0].initial_location

def into_clusters_by_points(pirates, options):
    """
    :param cluster:
    :param game: Game Object
    :param options:  Dictionary. {Location: Number of wanted pirates} and so on
    :return: Dictionary {Location: Cluster}
    """

    free_pirates = set(pirates)
    clusters = dict()  # {Location: Cluster}

    for location, number_of_wanted_pirates in options.items():
        close_pirates = sorted(free_pirates, cmp=lambda pirate: pirate.distance(location))
        close_pirates = close_pirates[:min(len(close_pirates), number_of_wanted_pirates)]
        clusters[location] = Cluster(close_pirates)
        free_pirates = free_pirates - set(close_pirates)
    clusters[Location(0,0)] = Cluster(free_pirates)
    return clusters

def get_entities_with_ids(entities, ids):
    matched_entities = []
    for entity in entities:
        if entity.id in ids:
            matched_entities.append(entity)
    return matched_entities

def center_of_mass(entities):
    row = 0
    col = 0
    for entity in entities:
        row += entity.location.row
        col += entity.location.col
    return pirates.Location(row/len(entities), col/len(entities))

def push_destination_to_edge(game, origin, has_capsule=False):
    '''
    Parameters
    ----------
    pirate : Pirate
        mostly enemy pirate
    game : Game
        game
    Returns
    -------
    The location to push a pirate of the map
    '''
    height = game.rows
    width = game.cols
    y = origin.row
    x = origin.col
    edges = [x, width-x, y, height-y]
    option = edges.index(min(edges))
    push_locations = [pirates.Location(y, -10), pirates.Location(y, width+10), pirates.Location(-10, x), pirates.Location(height+10, x)]
    if has_capsule and len(game.get_my_motherships())>0:
        col_center = game.get_my_motherships()[0].location.row
        unload_range = game.get_my_motherships()[0].unload_range
        # Check to see if we are not pushing the pirate with the capsule to the island
        if origin.row > col_center + unload_range or origin.row < col_center - unload_range:
            return push_locations[option]
        return pirates.Location(3000,3000)
    return push_locations[option]

def closest_locobj(locobjs, location, amount=-1, max_distance=-1):
    '''
    Get the closest locobj/s from location.
    Parameters
    ----------
    locobjs : list 
        List of objects with location
    location : Location
        The location
    amount : int
        return a maximum of amount objects. (default -1)
    max_distance : int
        max distance that a locobj can be from location. (default -1)
    Returns
    -------
    Returns a list of Location objects sorted by distance 
    from `location` that are not farther away than specified 
    by `max_distance`. (first is closest)
    '''
    close_locs = sorted(locobjs, key=lambda locobj: location.distance(locobj))
    if max_distance > -1:
        close_locs = filter(lambda locobj: location.in_range(locobj, max_distance), close_locs)
    if amount > -1:
        close_locs = close_locs[:min(amount, len(close_locs))]
    return close_locs

def get_closest_my_capsules(game, location):
    '''
    ----------
    game : GameObject
        game object
    Returns
    -------
    List of my capsules sorted by distance.
    '''
    return closest_locobj(game.get_my_capsules(), location)

def get_closest_capsules(game, location):
    '''
    ----------
    game : GameObject
        game object
    Returns
    -------
    List of enemy capsules sorted by distance.
    '''
    return closest_locobj(game.get_enemy_capsules(), location)


def get_closest_mothership(game, location):
    '''
    Gets the closest mothership from the location
    Parameters
    ----------
    game : GameObject 
        game object
    location : GameObject
        game object
    Returns
    -------
    Returns the closest mothership
    '''
    if not game.get_my_motherships():
        return None
    return sorted(game.get_my_motherships(), key=lambda x:x.distance(location))[0]

def get_threating_enemies(game, mothership):
    '''
    This function returns the closest threating enemy to a certain enemy mothership,
    with that information we can choose whether to protect this mothership or not.
    Parameters
    ----------
    game : GameObject
        game object- needed to get enemy_pirates_alive and enemy_capsules
    mothership : Mothership
        the enemy's mothership we want to get info for
    Returns
    -------
    Returns a list of tuples containing threating enemy_pirates and their distance from the mothership. 
    '''
    closest_capsule = get_closest_capsules(game, mothership)
    # Getting the closest capsule
    closest_capsule = closest_capsule[0]
    if closest_capsule.holder:
        # Getting close enemies to the capsule holder
        enemy_attack_group = closest_locobj(game.get_enemy_living_pirates(), closest_capsule.holder, max_distance=game.push_distance*2)
        return [(enemy_pirate, enemy_pirate.distance(mothership)) for enemy_pirate in enemy_attack_group]
    else:
        sm = globalvars.state_manager
        # Getting a list of enemies heading to the closest capsule
        enemies_taking_capsule = sm.get_enemies_heading_to(game, closest_capsule)
        # Getting the travel distance to the capsule and from the capsule to the closest mothership
        distance_capsule_to_mothership = closest_capsule.distance(mothership)
        return [(enemy_pirate, enemy_pirate.distance(closest_capsule) + distance_capsule_to_mothership) for enemy_pirate in enemies_taking_capsule]

def get_closest_wall(game, map_object):
    # returns location, so you can use the towards function to this location
    row = game.rows
    col = game.cols
    possible_wall_locations = [pirates.Location(0, map_object.location.col), 
                               pirates.Location(map_object.location.row, 0),
                               pirates.Location(row, map_object.location.col), 
                               pirates.Location(map_object.location.row, col)]
    return sorted(possible_wall_locations, key=lambda x:x.distance(map_object))[0]                               

def get_closest_enemy(game, map_object):
    return sorted(game.get_enemy_living_pirates(), key=lambda x: x.distance(map_object))[0]

def get_closest_pirate(game, map_object):
    return sorted(game.get_my_living_pirates(), key=lambda x: x.distance(map_object))[0]
    
def get_distance_to_closest_unload_range(mapobject, mothership, game):
    critical_dist = game.push_distance + game.mothership_unload_range
    return mapobject.distance(mothership)-critical_dist

def get_slope(location1, location2):
    # returns a slope (dy/dx) between location1 and location2
    if location1.col == location2.col or location1.row == location2.row:
        return 1
    return round((location1.row - location2.row)/(float(location1.col - location2.col)))

def get_num_enemys_at_range(location, radius, game):
    return len(get_enemies_at_range(location, radius, game))
    
def get_enemies_at_range(location, radius, game):
    enemies = []
    for enemy in game.get_enemy_living_pirates():
        if(location.in_range(enemy.location), radius):
            enemies.append(enemy)
    return enemies

def get_bridge_points(cluster, start_loc, end_location, game, critical_dist=True):
    radius = (game.push_distance + game.push_range + cluster[0].max_speed)/2 # CONSTANT can be changed, not recommended
    final_pts = []
    circle_centers = get_center_bridge_points(cluster, start_loc, end_location, game, critical_dist=True)
    for c_location in circle_centers:
        final_pts.append(get_circle_best_point(c_location, radius, game, mapobject=end_location))
    return final_pts

def get_center_bridge_points(cluster, start_loc, end_location, game, critical_dist=True):
    # returns a list of points for "jumping"
    # the carrier must be pushed by escorter
    
    center_pts = []
    if critical_dist:
        critical_dist = game.push_distance + game.mothership_unload_range
    else:
        critical_dist = 0
    delta_distances = 2 * (start_loc.distance(end_location)-critical_dist)/len(cluster.entities)
    for i in range(0, len(cluster.entities)): 
        if type(start_loc) == type(pirates.Location(0,0)):
            center_pts.append(start_loc.towards(end_location, delta_distances*i))
        else:
            center_pts.append(start_loc.location.towards(end_location, delta_distances*i))
    return center_pts

def get_circle_best_point(location, radius, game, mapobject=None):
    # This function gets one point on the perimeter of the circle 
    # created by the location=center of the circle and the given radius
    # notice: function only searches the points which are closer to the given
    #         mapobject if supplied 
    possible_locations = {}
    # initiate the locations in the dictionary

    delta_alpha = 10  # CONSTANT can be changed
    max_alpha = 360
    alpha = 0
    
    if mapobject:
        max_alpha -= 180  # CONSTANT can be changed, not recommended. Takes the closer half circle to the mapobject
        if type(mapobject) == type(pirates.Location(0,0)):
            alpha = math.degrees(math.atan(-1/get_slope(location, mapobject))) + 180    
        else:
            alpha = math.degrees(math.atan(-1/get_slope(location, mapobject.location))) + 180
       
    for i in range(0, max_alpha, delta_alpha):  # get points with 10 degrees difference between them        
        col = location.col + round(radius * math.cos(math.radians(alpha+i)))
        row = location.row + round(radius * math.sin(math.radians(alpha+i)))
        loc = pirates.Location(int(row), int(col))
        possible_locations[loc] = 0 # all locations are initialized with zero points
    
    print "CIRCLE LOCATIONS:"
    print possible_locations
            
    # evaluate each location and give it a score 
    possible_locations = set_score_per_location(possible_locations, game)
    return [key for (key, value) in sorted(possible_locations.items())][0]

def set_score_per_location(circle_locations, game):
    # returns updated dictionary with score for each location 
    # Score is evaluated as following (THE LOWER THE SCORE THE BETTER):
    # distance to closest enemy pirate * 2 +
    # distance to closest mothership_unload_range * 3 +
    # distance to closest my pirate * -2 +
    # NOTE: this function is far from finished, and does not work currently properly. needs to be reviewed 
    
    for loc in circle_locations:
        circle_locations[loc] = loc.distance(get_closest_enemy(game, loc)) * 100 + \
        get_distance_to_closest_unload_range(loc, game.get_my_mothership(), game) * 3 + \
        loc.distance(get_closest_pirate(game, loc)) * -1 
        
    for loc in circle_locations:
        circle_locations[loc] = loc.distance(game.get_my_mothership())

    return circle_locations
