import pirates
import MyBot
class TurnManager(object):
    def __init__(self, game):
        self.game = game
        self.enemy_pirate_push_ids = {} # int pirate id : int pushed
        self.pirate_push_ids = {}
        self.astroid_push_ids = {} # int astroid id : int pushed
        for p in game.get_all_enemy_pirates():
            self.enemy_pirate_push_ids[p.id] = 0
        for p in game.get_all_my_pirates():
            self.pirate_push_ids[p.id] = 0
        for a in game.get_all_asteroids():
            self.astroid_push_ids[a.id] = 0

    def set_push(self, pushable_mapobj):
        if type(pushable_mapobj) is MyBot.PirateDecorator:
            self.pirate_push_ids[pushable_mapobj.get_id()] += 1
        elif type(pushable_mapobj) is pirates.Asteroid:
            self.astroid_push_ids[pushable_mapobj.id] += 1
        elif type(pushable_mapobj) is pirates.Pirate:
            if pushable_mapobj in self.game.get_all_my_pirates():
                self.pirate_push_ids[pushable_mapobj.id] += 1
            elif pushable_mapobj in self.game.get_all_enemy_pirates():
                self.enemy_pirate_push_ids[pushable_mapobj.id] += 1

    def was_mapobj_pushed(self, pushable_mapobj, max_push=1):
        if type(pushable_mapobj) is MyBot.PirateDecorator:
            return self.pirate_push_ids[pushable_mapobj.get_id()] < max_push
        elif type(pushable_mapobj) is pirates.Asteroid:
            return self.astroid_push_ids[pushable_mapobj.id] < max_push
        elif type(pushable_mapobj) is pirates.Pirate:
            if pushable_mapobj in self.game.get_all_my_pirates():
                return self.pirate_push_ids[pushable_mapobj.id] < max_push
            elif pushable_mapobj in self.game.get_all_enemy_pirates():
                return self.enemy_pirate_push_ids[pushable_mapobj.id] < max_push
